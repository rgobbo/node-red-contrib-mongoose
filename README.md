# node-red-contrib-mongoose
Elegant mongodb object modeling interface for Node-RED

based on [Mongoose](https://https://mongoosejs.com/) 

Please refer to the [mongooose documentation](http://https://mongoosejs.com/docs/guide.html) to read about each operation.

# Installation

Install `node-red-contrib-mongoose` using [npm](https://www.npmjs.com/)

```bash
npm install --save node-red-contrib-mongoose
```

# Usage

**Mongoose Model**
Node based on mongoose to define a model, with type and validation that will be used in MOngooseOperation NOde

**msg.payload** - 
* To pass a single parameter to an operation use `msg.payload` as your parameter (eg `{_id: 1243}`)
* To pass multiple parameters to an operation fill `msg.payload` with an array 
* If you want to pass a single parameter WHICH IS AN ARRAY (eg as with `InserMany`), wrap your array in an outer array: `msg.payload` = `[[{_id: 1243}, {_id: 2345}]]`


**URI** -
Using a single URI field allows you to specify Host, Port and Database configuration as well as all other features that are supported by the [MongoClient.connect](http://mongodb.github.io/node-mongodb-native/2.1/api/MongoClient.html#.connect), such as Mongos Proxy Connection and Replicaset Server Connection - more information can be found [here](http://mongodb.github.io/node-mongodb-native/2.0/tutorials/connecting).
Notice that the Username & Password fields did remain. They will still be saved as Node-RED credentials (i.e. kept private). If the Username is not empty or the Password is not empty, they will be escaped and added the the URI after the `mongodb://` prefix (separated by ':' and with a '@' after them). You can also leave these fields empty and enter the credentials directly in the URI, following the standard syntax: `mongodb://youruser:yourpassword@host1.yourmongoprovider.com:27017,host2.yourmongoprovider.com:27017/yourdb?replicaSet=foo`. **Do not enter your credentials both in the URI and the Username & Password fields** - this will create an invalid URI such as: `mongodb://youruserfromfield:yourpasswordformfield@youruserfromuri:yourpasswordfromuri@host1.yourmongoprovider.com:27017,host2.yourmongoprovider.com:27017/yourdb?replicaSet=foo`.

**specifying authentication database** - 
most recent deployments of mongoDB store user credentials in a separate databse (usually `admin`) rather than allongside the data in each Db. Therefore you will likley need to provide a `authSource` parameter in your URI
eg: `mongodb://host1.yourmongoprovider.com:27017/yourdb?ssl=true&authSource=admin&replicaSet=foo`


**db & collection operations** - These operations will simply pass the db/collection instance, so they can be used directly (for example, in function nodes).
The db instance is the same one that node-red-contrib-mongodb3 caches and shares between all relevant nodes - if you disconnect it, all the other mongodb3 nodes will fail.
Furthermore, the parallelism-limit does not consider the future operations that you will do with the db/collection instances.
However, if there are many parallel operations, requesting the db/collection will block until some of these operations finish.

# Change Log

